

/*Inside of routes.js, create an exportable function that will handle 
different endpoints for our server:

For the "/" endpoint, display a message saying "This is the homepage"

For the "/login" endpoint, display a message saying "This is the login page."

For the "/about" endpoint, display a message saying "This is the about page."

For any other pages, display a message saying "Page not found with a 404 status code"

Import that function into index.js and add it to the createServer() method

*/

const http = require("http");


const importIN = require("./routes");


const server = http.createServer(importIN.requestListener)

const port = 5000
server.listen(port) // If user try to communicate with port 3000, send the response that we added in createServer()

console.log(`Server running at port 5000`) //Confirmation that server is running
