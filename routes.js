

module.exports.requestListener = function (request, response) {
  
  if(request.url === '/'){
       response.writeHead(200, {'Content-Type': 'text/plain'})
        response.end("This is the homepage")
  } else if(request.url === '/login'){
       response.writeHead(200, {'Content-Type': 'text/plain'})
        response.end("This is the login page.")
 } else if(request.url === '/about'){
       response.writeHead(200, {'Content-Type': 'text/plain'})
        response.end("This is the about page.")
 } else {
     response.writeHead(404, {'Content-Type': 'text/plain'})
    response.end("Page not found")

 }
 
}